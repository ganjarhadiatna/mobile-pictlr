<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');
Route::get('/{any}', 'MainController@index')
    ->where(['any', '*']);
Route::get('/{any}/{any2}', 'MainController@index')
    ->where(['any', '*'], [ 'any2', '*']);
Route::get('/{any}/{any2}/{any3}', 'MainController@index')
    ->where(['any', '*'], [ 'any2', '*'], [ 'any3', '*']);
Route::get('/{any}/{any2}/{any3}/{any4}', 'MainController@index')
    ->where(['any', '*'], [ 'any2', '*'], [ 'any3', '*'], [ 'any4', '*']);
Route::get('/{any}/{any2}/{any3}/{any4}/{any5}', 'MainController@index')
    ->where(['any', '*'], [ 'any2', '*'], [ 'any3', '*'], [ 'any4', '*'], [ 'any5', '*']);
Route::get('/{any}/{any2}/{any3}/{any4}/{any5}/{any6}', 'MainController@index')
    ->where(['any', '*'], [ 'any2', '*'], [ 'any3', '*'], [ 'any4', '*'], [ 'any5', '*'], [ 'any6', '*']);



